﻿import shutil 
import errno
import SSGConfyg
import SSGController
import os


'''
 Class FileManager
    gets a list of all markdown files and files to copy on inicialization
    @param - direcroty of the root of a project

'''

class FilesManager(object):
    
    def __init__(self, dir, confyg):
        
        self.dir= dir
        self.markdown_files = []
        self.files=[]

      
        dir=dir+os.sep+"content"
        for root, subFolder, files in os.walk(dir):
            for item in files:
                if item.endswith(".md"):
                    self.markdown_files.append(os.path.join(str(root),item))
                else:
                    if not confyg.ignore_extantions(item) and not confyg.ignored_files(item):                      
                        self.files.append(os.path.join(str(root),item))       

    def to_string(self):
        print("Coppy files ", self.copy_files)
        print("Markdown files ", self.markdown_files)

    '''
    Coppy all files, besides ihnored,  to WWW directory

    '''

    def copy_files(self, src):
        dest=self.dir+os.sep+'www'+os.sep+os.path.dirname(src).split('content')[1]

        if not os.path.isdir(dest): 
             os.makedirs(dest)
        try:
             shutil.copytree(src, dest)
        except OSError as e:
            # If the error was caused because the source wasn't a directory
            if e.errno == errno.ENOTDIR:
                shutil.copy(src, dest)
            else:
                print('Directory not copied. Error: %s' % e)

    # Remuves build if error occurs
    def delete_build(self):
        shutil.rmtree(self.dir+os.sep+'www')

        