﻿import os

class SSGConfyg(object):

    def __init__(self, path):
        confyg_file=open(path+os.sep+'.ssg', "r").read().split('\n')
        
        #ddd ignore extantion 

        def ig_ext(line):
            self.extantions=line[line.index("[") + 1:line.rindex("]")].split(",")

        #ignore files

        def ig_file(line):
            self.files=line[line.index("[") + 1:line.rindex("]")].split(",")

        #Header
        def default_h(line):
             self.header=line[line.index("[") + 1:line.rindex("]")].split(",")[0]

        #Footer
        def default_f(line):
             self.footer=line[line.index("[") + 1:line.rindex("]")].split(",")[0]
        
        def all_menues(line):
            self.nav_bar= line[line.index("[") + 1:line.rindex("]")].split(",")    

        options = {
           'Ignore Extensions' : ig_ext,
           'Ignore Files' : ig_file,
           'Default Header' : default_h,
           'Deafault Footer' : default_f,
           'NavBar': all_menues
           }
           
        for line in confyg_file:
                
            tolken=line.split(':')[0]
            if not tolken == None:   
                try:              
                    options[tolken](line)
                except KeyError: continue

    def ignore_extantions(self, file):
        file=file.strip()
        for ext in self.extantions:
            if os.path.splitext(file)==ext.strip():
                return True
        return False

    def ignored_files(self, file):

        file=file.strip()

        for f in self.files:
            if f.strip()== file:
                return True
        return False

    def get_header(self):
        return self.header

    def get_footer(self):
        return self.footer

    def get_nav_bar(self):
        return self.nav_bar
