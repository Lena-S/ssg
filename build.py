﻿
import os
import find_root
import SSGFileManager
import SSGConverter
import SSGController

def run(): 

   #dir=find_root.site_directory()  #when run from command line  
   dir = os.getcwd()+os.sep+"MINI"  # when debuging

   Controller=SSGController.Controller()
   Controller.init_all(dir)

   print("Building site in ", dir)
   for item in Controller.get_file_manager().files:
       Controller.get_file_manager().copy_files(item) 
   for item in Controller.get_file_manager().markdown_files:
       Controller.get_converter().gen_html(item,dir)

   


