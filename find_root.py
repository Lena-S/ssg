﻿#!/usr/bin/env/python

from __future__ import print_function
import os

def site_directory():

    """
    Return the path of the innermost SSG directory as determined
    by the presence of a .ssg file.
    """

    cwd = os.getcwd()
    parent_dirs = cwd.split(os.sep)
    for i in range(len(parent_dirs)):
        dir = os.sep.join(parent_dirs[:len(parent_dirs) - i])
        ssg_file = os.sep.join([dir, '.ssg'])
        if os.path.isfile(ssg_file):
            return dir
    return None

def project_directory(cwd):

    """
    Return the path of the innermost SSG directory as determined
    by the presence of a .ssg file.
    """
    parent_dirs = cwd.split(os.sep)
    for i in range(len(parent_dirs)):
        dir = os.sep.join(parent_dirs[:len(parent_dirs) - i])
        ssg_file = os.sep.join([dir, 'SSG.exe'])
        if os.path.isfile(ssg_file):
            return dir
    return None


