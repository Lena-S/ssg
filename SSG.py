﻿#!/usr/bin/env python
import sys
import os
import build
import markdown
import argparse
import create


menu={
    'create': create.run,    
    'build': build.run
    }

def main():
    #take all 
    parser = argparse.ArgumentParser()
    parser.add_argument("action", help="-create or build project", choices=['create', 'build'])    
    parser.add_argument("-n","--name", help="-name of a projet for create action", type=str)
   
    args=parser.parse_args()
    if args.action=='create':
        if args.name is None:
            parser.error("-n 'Name' and -action='create' must be given together \n Example: ssg.py -n MySite create")
        menu[args.action](args.name)
    else: menu[args.action]()
    
        
if __name__ == '__main__':  
  
    main()
