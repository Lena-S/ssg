﻿from distutils.core import setup
import py2exe
import os
demo=[]

def get_demo(path, Mydata_files):
    for files in os.listdir(path):
         f1 = path+os.sep + files         
         if os.path.isfile(f1): # skip directories
             dirname=os.path.dirname(f1).split("demo")[1]
             f2 = "demo"+ os.sep+dirname, [f1]
             Mydata_files.append(f2)
         else:
             get_demo(f1, Mydata_files)  
    return Mydata_files

Mydata_files=[]
Mydata_files = get_demo(os.getcwd()+ os.sep+"demo", Mydata_files)

setup(
     name="SSG?",
     version="1.0",
     author="Lena A-Sabino & Confer?",
     console=['SSG.py'],
     data_files = Mydata_files )