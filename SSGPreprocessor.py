﻿import re
import sys
import SSGFileManager


from markdown.preprocessors import Preprocessor

class MarkdownPreprocessor(Preprocessor):
    def __init__(self, controller):
        self.controller=controller

    def run(self, file):
        lines=file.split("\n")
        new_lines = []
        for i, line in enumerate(lines):
            m = re.search(r'\(\(\(([^)]+)\)\)\)', line,0)
            if m:
                #print("String is ", m.groups())
                #print("Found it on line ", lines[i] )# do stuff
                dot_error=re.search(r'(\.+\..*)', m.groups()[0], 0)

                if not dot_error == None:                 

                    if m.group()[0].startswith('/'):
                        lines[i]=lines[i].replace( "((("+m.groups()[0]+")))", "(Haha)")
                        #print("after replacment ", lines[i] )# do stuff             
                        new_lines.append(lines[i])

                    if m.group()[0].startswith('//'):
                        print('********  //  *************')


                    

                else:
                    print ("Error ", dot_error.group()[0]," not suported")                   
                    self.controller.get_file_manager().delete_build()
                    sys.exit(0)
            else:
                new_lines.append(line)
        return new_lines