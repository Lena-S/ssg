﻿import SSGPreprocessor 
import SSGController
import markdown
import os
import re

class FileConverter(object):

    def __init__(self, controller):
        self.controller=controller
    '''
    Function get_html converts markdown files to htm
    @param- file to convert
    @param- direcrory where to put html files
    '''
    def gen_html(self,file, dir):
        
        NavBars=[]     

        #Header
        if os.path.isfile(dir+os.sep+'content'+os.sep+self.controller.get_confyg().get_header()): Header=open(dir+os.sep+'content'+os.sep+self.controller.get_confyg().get_header()).read()
        else: print("File name ",dir+os.sep+'content'+os.sep+self.controller.get_confyg().get_header(), " not found\n" )
        #Footer
        if os.path.isfile(dir+os.sep+'content'+os.sep+self.controller.get_confyg().get_footer()): Footer=open(dir+os.sep+'content'+os.sep+self.controller.get_confyg().get_footer()).read()
        else: print("File name ",dir+os.sep+'content'+os.sep+self.controller.get_confyg().get_footer(), " not found\n" )

        #Menues
        if not self.controller.get_confyg().get_nav_bar() == None:
            for nb in self.controller.get_confyg().get_nav_bar():
                 if os.path.isfile(dir+os.sep+'content'+os.sep+nb): NavBars.append(open(dir+os.sep+'content'+os.sep+nb).read())
                 else: print("File name ",dir+os.sep+'content'+os.sep+nb, " not found\n" )
        

        dest=dir+os.sep+'www'+os.sep+os.path.dirname(file).split('content')[1]     
        dir=dir+os.sep+'www'

        #Run markdown file through preprocessor
        content_file= open(file, "r+")
        content=content_file.read()
        processed_text=self.controller.gef_preprocessor().run(content)

        #Overide file with a file returned from preprocessor
        content_file.seek(0)
        s="\n".join(processed_text)
        content_file.write(s)
        content_file.truncate()
       
        md = markdown.Markdown(extensions = ['markdown.extensions.meta'])
        
        #Make WWW directory in the root of the proect
        if not os.path.exists(dest):
                os.makedirs(dest)
     
        html = md.convert(content)     
       
        if 'title' in md.Meta:
            Header=re.sub(r"<title>.*<\/title>", "<title>"+''.join(md.Meta['title'])+"</title>", Header)

        if (os.path.splitext(os.path.basename(file))[0]=='index'):
            output=open(dest+os.sep+"index.html", "w")
        else:
            new_dir=dest+os.sep+os.path.splitext(os.path.basename(file))[0]
            if not os.path.exists(new_dir):
                os.makedirs( new_dir)
            output=open( new_dir+os.sep+"index.html", "w")    

        if not Header == None: output.write(Header)

        if not  NavBars == None:
            for nb in NavBars:
                if not nb == None: output.write(nb)               
        output.write(html)

        if not Footer == None:output.write(Footer)
        
        content_file.close()
        output.close()

