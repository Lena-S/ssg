﻿import SSGFileManager
import SSGConfyg
import SSGConverter
import SSGPreprocessor

'''
    Wrapper class containce instances of all classes
'''

class Controller(object):
    
    def __init__(self):
        self.file_manager=None
        self.converter=None
        self.confyg=None
        self.preprocessor=None

    def init_all(self, dir):
       
        self.converter=SSGConverter.FileConverter(self)
        self.confyg=SSGConfyg.SSGConfyg(dir)
        self.preprocessor=SSGPreprocessor.MarkdownPreprocessor(self)
        self.file_manager= SSGFileManager.FilesManager(dir, self.confyg)

    def get_file_manager(self):
        return self.file_manager

    def get_converter(self):
        return self.converter

    def get_confyg(self):
        return self.confyg

    def gef_preprocessor(self):
        return self.preprocessor
