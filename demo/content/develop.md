title: MiniAT | Develop
<div class="container">
	<div class="page-header">
        <h1> Develop / Contribute  </h1>
	</div>

	<div id="sitemap_nav">
		<p><strong><a href="/develop" >Develop / Contribute</a></strong></p>
	</div>
	<p>The <strong>MiniATeam</strong> is always looking to identify excited and capable users who want more
		and are willing to participate at the community level.  Below is a small list of sub-projects
		<em>we</em> would love to see happen:
	</p>
	<ul>
	<li>True end-user documentation, both for our system developers and assembly programmers</li>
	<li>Updated packaging scripts to prepare prebuilt binaries for Linux32/64, Mac OS X, and Win32/64 users</li>
	<li><p>A completely revisited <strong><em>mash</em></strong> assembly language and improved assembler implementation</p></li>
	<li><p>Fun, graphical systems that are <strong><em>FUN</em></strong> to write assembly code to control.  For example,</p>
		<ul>
			<li>A multi-color plotter-printer with independently controlled motors for X and Y movements</li>
			<li>A robotic racing game where players&#8217; assembly programs race cars powered by the <strong>MiniAT</strong> 
				around various tracks.  Competition could be for best time against other bots or best time against your
				own previous best time</li>
			<li>A Multi-color LCD advertisement display (like the kinds in restaurant windows) with programmable
			content, fonts, effects, and transitions</li>
			<li>etc.</li>
		</ul>
	</li>
	</ul>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Give back and be FAMOUS!!!</h3>
		</div>
		<div class="panel-body">
			<p>
				To contribute to the <strong>MiniAT</strong> project, visit our source repository on Bitbucket and read their 
				pages on getting started with Bitbucket projects.
			</p>
			<p>
				<a class="btn btn-success btn-lg" role="button" href="https://bitbucket.org/miniat/0x1-miniat/src">MiniAT on Bitbucket</a>
				<a class="btn btn-warning btn-lg" role="button" href="https://confluence.atlassian.com/display/BITBUCKET/Bitbucket+101">Bitbucket 101</a>
			</p>
		</div>
	</div>

</div>

   