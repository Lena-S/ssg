title: MiniAt | Build

<div class="container">
    <div class="page-header">
        <h1>       Building the MiniAT from source        </h1>
    </div>
	<div id="sitemap_nav">
		<p><strong><a href="/getting-started" >Getting Started</a></strong></p>
		<ul>
			<li><a href="/getting-started/build" >Building the MiniAT from source</a></li>
			<li><a href="/getting-started/running-an-example" >Running an Example</a></li>
			<li><a href="/getting-started/debuging-with-your-favorite-ide" >Debuging with your favorite IDE</a></li>
		</ul>
	</div>
    <h2>Requirements</h2>
	<ol>
		<li><strong>A working C toolchain including <em>make</em>:</strong><br />
			To date, development has been done almost exclusively with <a href="http://gcc.gnu.org" title="The GNU Compiler Collection">GCC</a> on Linux and OSX.
			Other platforms and compilers are likely to be simple to work with, as well.
			One of the MiniATeam&#8217;s goals for the near future is to get building on Windows
			with <a href="http://gcc.gnu.org" title="The GNU Compiler Collection">GCC</a> and on OSX with <em>clang</em><sup id="fnref:1"><a href="#fn:1" rel="footnote">1</a></sup>.</li>
		<li><strong>scons</strong></li>
		<li><strong>python</strong></li>
		<li><strong>flex</strong></li>
	</ol>
	<h2>Build</h2>
	<ul>
		<li><a href="#ubuntu">Building with Ubuntu</a></li>
		<li><a href="#osx">Building with Mac OS X</a></li>
		<li><a href="#windows">Building with Windows</a></li>
	</ul>
	<p><a name="ubuntu"></a></p>
	<h3>Building with Ubuntu</h3>
	<p>Ensure all required tools are installed:</p>
	<pre><code>sudo apt-get install make gcc scons python flex
	</code></pre>
	<p>To build the MiniAT library documentation locally and run automated tests, you
		can optionally install <em>doxygen</em> and <em>sqlite3</em>.  This is unnecessary for typical
		use:</p>
	<pre><code>sudo apt-get install doxygen sqlite3
	</code></pre>
	<p>Change your working directory to the root of your MiniAT source package.  Based 
		on the examples shown prior, the command below assumes this is the <code>~/miniat</code>
		directory.</p>
	<pre><code>scons
	</code></pre>
	<p>After a successful build, you will have MiniAT system and development tool executables in <code>out/exe</code>, MiniAT and peripheral 
		libraries in <code>out/lib</code>, and assembled binary images to run on the example systems 
		in <code>out/bin</code>.</p>
	<p><a href="#next"><strong><em>Next >>></em></strong></a></p>
	<p><a name="osx"></a></p>
	<h3>Building with Mac OS X</h3>
	<p>To build the MiniAT, you will require <code>make</code>, <code>gcc</code>,
	<code>flex</code>, <code>scons</code>, and <code>python</code>.  The first three of 
		these are most easily installed via Apple&#8217;s XCode
		command line tools. From a terminal, exectute the
		following command:</p>
	<pre><code>xcode-select --install
	</code></pre>
	<p>A dialog will indicating you need the command line
		developer tools.  Click the &#8220;Instal&#8221; button.</p>
	<h4>Python and SCons</h4>
	<p>Download and install the most recent 2.x Python version 
		from the link below.  At the time of this writing, 
		Scons only works with Python 2.x, but that will change 
		with its next release.</p>
	<p><strong>Python:</strong> <a href="https://www.python.org/downloads/">https://www.python.org/downloads/</a></p>
	<p>Next, download and decompress SCons from the link below:</p>
	<p>SCons: <a href="http://sourceforge.net/projects/scons/files/latest/download?source=files">http://sourceforge.net/projects/scons/files/latest/download?source=files</a></p>
	<p>Change your working directory to the root of the SCons folder.
	<strong>If you have administrative privileges on your system</strong>, you can install SCons with the command below.
		Otherwise, follow the instructions for building and install SCons in its 
	<a href="http://www.scons.org/doc/production/HTML/scons-user.html#idp81344">user manual</a></p>
	<pre><code>sudo python setup.py install
	</code></pre>
	<h4>Build</h4>
	<p>To build the MiniAT library documentation locally and run automated tests, you
		can optionally install <em>doxygen</em> and <em>sqlite3</em>.  This is unnecessary for typical
		use and is not covered here but is on our TODO list.</p>
	<p>Change your working directory to the root of your MiniAT source package.  Based 
		on the examples shown prior, the command below assumes this is the <code>~/miniat</code>
		directory.</p>
	<pre><code>scons
		</code></pre>
	<p>After a successful build, you will have MiniAT system and development tool executables in <code>out/exe</code>, MiniAT and peripheral 
		libraries in <code>out/lib</code>, and assembled binary images to run on the example systems in <code>out/bin</code>.</p>
	<p><a href="#next"><strong><em>Next >>></em></strong></a></p>
	<p><a name="windows"></a></p>
	<h3>Building with Windows</h3>
	<p>To build the MiniAT, you will require <code>make</code>, <code>gcc</code>, <code>flex</code>, <code>python</code>, and <code>scons</code>.  Most
		of these can be installed as a part of MinGW, a native Windows port of the GNU
		compiler collection.</p>
	<h4>MinGW</h4>
	<p>Download the MinGW installer from the link below:</p>
	<p>MinGW <a href="http://sourceforge.net/projects/mingw/files/latest/download?source=files">http://sourceforge.net/projects/mingw/files/latest/download?source=files</a></p>
	<p>When you run the MinGW installer, you will need to choose an installation directory.
		For this guide, it is assumed the default location (<code>c:\mingw</code>) is chosen.  When 
		the Installation Manager window opens, select &#8220;All Packages&#8221; from the left pane.
		In the right pane, select the following packages:</p>
	<pre><code>mingw32-gcc mingw32-make mingw32-base msys-flex
	</code></pre>
	<p>Once the packages are selected, go to the Installation menu at the top of the 
		window and select &#8220;Apply Changes&#8221;.  On occasion, an error could appear during this
		step indicating a package could not be downloaded.  If this happens, wait a minute
		and select Apply Changes again.  Once the MinGW installer is complete, close it.</p>
	<p>To complete the MinGW setup, you will need to add the MinGW binary directories 
		to your runtime path and create a <code>make.exe</code> in one of them (explained momentarily).
		First, click the Start button and type <code>environment</code> into the search box that appears
		directly above the start button.  Select &#8220;Edit environment variables for your account&#8221;
		from the menu that appears.  In the &#8220;User variables for&#8230;&#8221; section at the top
		of the &#8220;Environment variables&#8221; window, edit the <code>Path</code> variable (if it already exists)
		to end with <code>;c:\mingw\bin;c:\mingw\msys\1.0\bin</code> (notice the semicolon at the 
		beginning?).  If <code>Path</code> is not already a user variable, create it by clicking 
		the <strong>New</strong> button, entering <code>Path</code> for variable name and 
		<code>c:\mingw\bin;c:\mingw\msys\1.0\bin</code> for variable value (no semicolon at the 
		beginning needed here).  In this example, the MSys version is 1.0, if yours is
		different, you&#8217;ll need to account for that in your path values.</p>
	<p>Finally, from your file explorer, open the <code>c:\mingw\bin</code> directory.  Make a
		copy of <code>mingw32-make.exe</code> (you might not see the <code>.exe</code> extension if your
		explorer has the default setting to hide file extensions) in that same directory
		and rename it <code>make.exe</code>.  Remember to <strong>COPY</strong>&#8230; you will still need the 
		original file as it is used by name by some MinGW tools.</p>
	<h4>Python and SCons</h4>
	<p>Download and install the most recent 2.x Python version from the link below.<br />
		At the time of this writing, Scons only works with Python 2.x, but that will
		change with its next release.</p>
	<p><strong>Python:</strong> <a href="https://www.python.org/downloads/">https://www.python.org/downloads/</a></p>
	<p>Next, download and install SCons from the link below:</p>
	<p>SCons: <a href="http://sourceforge.net/projects/scons/files/latest/download?source=files">http://sourceforge.net/projects/scons/files/latest/download?source=files</a></p>
	<p>Click the Start button and type <code>environment</code> into the search box that appears
		directly above the start button.  Select &#8220;Edit environment variables for your account&#8221;
		from the menu that appears.  In the &#8220;User variables for&#8230;&#8221; section at the top
		of the &#8220;Environment variables&#8221; window, edit the <code>Path</code> variable (if it already exists)
		to end with <code>;c:\python27;c:\python27\scripts</code> (notice the semicolon at the 
		beginning?).  Your Python directory may be different dependent on version.</p>
	<h4>Build</h4>
	<p>To build the MiniAT library documentation locally and run automated tests, you
		can optionally install <em>doxygen</em> and <em>sqlite3</em>.  This is unnecessary for typical
		use and is not covered here but is on our TODO list.</p>
	<p>This next bit is copy-and-paste from above and is not <em>really</em> for Windows. TODO :)</p>
	<p>Change your working directory to the root of your MiniAT source package.  Based 
		on the examples shown prior, the command below assumes this is the <code>~/miniat</code>
		directory.</p>
	<pre><code>scons
	</code></pre>
	<p>After a successful build, you will have MiniAT system and development tool executables in <code>out/exe</code>, MiniAT and peripheral 
		libraries in <code>out/lib</code>, and assembled binary images to run on the example systems 
		in <code>out/bin</code>.</p>
	<p><a name="next"></a></p>
	<h2>Next?</h2>
	<p>Once your build is completing successfully, move on to <a href="http://www.miniat.org/getting-started/running-an-example">running an example program</a>.</p>
	<div class="footnotes">	
		<ol>
			<li id="fn:1">
				<p>Recently, Apple has stopped packaging GDB (the Gnu Debugger) with the command
					line development tools provided through XCode in favor of <em>clang</em>.&#160;<a href="#fnref:1" rev="footnote">&#8617;</a></p>
			</li>
		</ol>
	</div>
</div>

	