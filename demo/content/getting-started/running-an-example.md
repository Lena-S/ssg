title: MiniAT | Running an Example    

<div class="container">
    <div class="page-header">
        <h1>          Running an Example        </h1>
    </div>
	<div id="sitemap_nav">
		<p><strong><a href="/getting-started" >Getting Started</a></strong></p>
		<ul>
			<li><a href="/getting-started/build" >Building the MiniAT from source</a></li>
			<li><a href="/getting-started/running-an-example" >Running an Example</a></li>
			<li><a href="/getting-started/debuging-with-your-favorite-ide" >Debuging with your favorite IDE</a></li>
		</ul>
	</div>
    <p>There are some slight differences in how you run a MiniAT program from operating system to operating system.  These differences will be highlighted below, but, for the moment,</p>
	<h2>Running Example From Source On Ubuntu and Mac OS X</h2>
	<p>Open a command terminal and change to the root of your miniat project. For the sake of this example, it is assumed your MiniAT source tree/installation is in <code>~/miniat/</code>.  It is also assumed you have already built the miniat from source code&#8230; if not, follow the tutorial on <a href="http://www.miniat.org/getting-started/build">building the Miniat from source</a>.</p>
	<pre><code>cd ~/miniat</code></pre>
	<h3>Update your runtime path and library path</h3>
	<p>On an Ubuntu Linux system with a BASH shell:</p>
	<pre><code>export PATH=$PATH:~/miniat/out/exe export LD_LIBRARY_PATH=~/miniat/out/lib</code></pre>
	<p>On a standard Mac OS X terminal:</p>
	<pre><code>export PATH=$PATH:~/miniat/out/exe export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:~/miniat/out/lib
	</code></pre>
	<p>Regardless of operating system and terminal environment, commands such as the ones above may only apply temporarily.  In general it is wise to add commands like these to your user account startup/terminal scripts (e.g., <code>.bashrc</code> or <code>.profile</code>) so you don&#8217;t have to repeat this over and over each time you start a new terminal session.</p>
	<h3>Run the example</h3>
	<p>Since the simulated systems using the MiniAT typically simulate a stand-alone embedded system, the simulator continues executing clock cycles forever.  On Unix-like systems including Linux and Mac OS X, pressing ctrl + c will usually exit the application.</p>
	<pre><code>miniat_console ./out/bin/simple_term.bin
	</code></pre>
	<p>This simple example executes the MiniAT console system simulation as if it were preloaded with the ROM image of the <code>./system/console/simple_term.asm</code> source.  The MiniAT console system is nothing more than a simulation of a simple computer running on the MiniAT embedded microcontroller with only a screen and keyboard as peripherals.  The <code>simple_term</code> code, in this case at least, makes this system operate as a trivial echo, but anyone can write other software to make this same system engage the user to play Tic-Tac-Toe, for example.</p>
</div>

 