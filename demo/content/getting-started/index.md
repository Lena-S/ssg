title: MiniAT | Getting Started    

<div class="container">
    <div class="page-header">
        <h1>         Getting Started        </h1>
    </div>
	<div id="sitemap_nav">
		<p><strong><a href="/getting-started" >Getting Started</a></strong></p>
		<ul>
			<li><a href="/getting-started/build" >Building the MiniAT from source</a></li>
			<li><a href="/getting-started/running-an-example" >Running an Example</a></li>
			<li><a href="/getting-started/debuging-with-your-favorite-ide" >Debuging with your favorite IDE</a></li>
		</ul>
	</div>      
</div>

  