title: MiniAT | Debugging with your favourite IDE

<div class="container">
    <div class="page-header">
        <h1>       Debuging with your favorite IDE        </h1>         
    </div>
	<div id="sitemap_nav">
		<p><strong><a href="/getting-started" >Getting Started</a></strong></p>
		<ul>
			<li><a href="/getting-started/build" >Building the MiniAT from source</a></li>
			<li><a href="/getting-started/running-an-example" >Running an Example</a></li>
			<li><a href="/getting-started/debuging-with-your-favorite-ide" >Debuging with your favorite IDE</a></li>
		</ul>
	</div>
    <p>Debugging unexpected behavior with your <strong>MiniAT</strong> program can be a little overwhelming at first, but don&#8217;t let it get to you.  After all, a running <strong>MiniAT</strong> system has many moving parts:</p>
	<ul>
		<li>the <strong>MiniAT</strong> library that simulates the underlying microcontroller</li>
		<li>system code that provides clock cycles and peripheral devices interfacing the user and <strong>MiniAT</strong></li>
		<li>and the user-provided program image  </li>
	</ul>
	<p>You can debug using your favorite IDE once you&#8217;ve determined how to load your work into its project structure and modify the project settings appropriately.  In this tutorial, we will show how this can be done using three popular cross-platform IDEs: Qt Creator, NetBeans, and Eclipse.</p>
	<p>For the sake of a common project baseline, these walkthroughs will debug the <code>miniat_console</code> system running the provided <code>simple_term.bin</code>program image.</p>
	<div class="alert alert-warning" role="alert">It is assumed you have the root of your <strong>MiniAT</strong> source tree installed at <code>~/miniat</code>.  For these instructions, this will explicitly be <code>/home/amos/miniat</code>
	</div>
	<h3>Debugging with Qt Creator</h3>
	<p>These insructions were written for Qt Creator version 3.2.1, so the precise details necessary for this to work for you may vary slightly from the text below.</p>
	<ol>
		<li><p>Create a new project with the &#8220;Import Existing Project&#8221; template.  It&#8217;s one of the &#8220;Import Project&#8221; types</p></li>
		<li><p>Give your project a name that&#8217;s easily distinguishable from the <strong>MiniAT</strong> files, like &#8220;miniat_creator&#8221; and set its location to the root of your miniat source tree&#8230; e.g., <code>/home/amos/miniat</code>.</p></li>
		<li><p>On the &#8220;File Selection&#8221; screen, you&#8217;ll need to ensure all the SCons files are included in the import.  The easiest way to do this is to append the &#8220;Show files matching:&#8221; field with the string <code>; SConstruct; SConscript</code>, and click the &#8220;Apply Filter&#8221; button.  If the <code>out</code> folder is selected because you started with an uncleaned tree, deselect it before clicking &#8220;Next&#8221;.</p></li>
		<li><p>For the sake of simplicity, we will turn of version control by setting that field to <code>&lt;None&gt;</code> before clicking &#8220;Finish&#8221;.</p></li>
	</ol>		
	<p>Now that the project exists, we just need to adjust a few of the default settings to get Qt Creator to work with our SCons build system and run/debug the <strong>MiniAT</strong> Console.</p>
	<ol>
		<li><p>Select the &#8220;Projects&#8221; tab from the mode selector running vertically on the left to access the build settings.</p></li>
		<li><p>Eliminate the use of Make by clicking the &#8220;X&#8221;s that appear when you hover your mouse at the end of the &#8220;Build Steps&#8221; and &#8220;Clean Steps&#8221; fields (near the &#8220;Details&#8221; dropdowns).</p></li>
		<li><p>Add the &#8220;Custom Process&#8221; build step with the command <code>scons</code> and the argument <code>--debugging</code>.</p></li>
		<li><p>Add the &#8220;Custom Process&#8221; clean step with the command <code>scons</code> and the argument <code>-c</code>.</p></li>
		<li><p>In the desktop selector at the top of the &#8220;Build &amp; Run&#8221; tab, select &#8220;Run&#8221; to access the run settings.</p></li>
		<li><p>Set the run executable to point to <code>miniat_console</code>&#8230; e.g., <code>/home/amos/miniat/out/exe/miniat_console</code>.</p></li>
		<li><p>Set the argument to point to the <code>simple_term</code> binary image&#8230; e.g., <code>/home/amos/miniat/out/bin/simple_term.bin</code>.</p></li>
		<li><p>Make sure the &#8220;Run in terminal&#8221; checkbox is is checked.</p></li>
		<li><p>Finally, click the &#8220;Details&#8221; dropdown of the &#8220;Run Enviorment&#8221; to add the variable <code>LD_LIBRARY_PATH</code> with the value that points to the <strong>MiniAT</strong> generated libraries&#8230; e.g., <code>/home/amos/miniat/out/lib</code>.</p></li>
	</ol>
	<p>To test your setup, switch to the editor, add a breakpoint somewhere in <code>system/console/console.c</code>, clean and build, and click the debug button on the bottom left of the mode selector pane.</p>
	<h3>Debugging with NetBeans IDE</h3>
	<p>These insructions were written for NetBeans IDE version 8.0.1, so the precise details necessary for this to work for you may vary slightly from the text below.  It is further assumed you already have C/C++ project suport either because you initially downloaded the NetBeans with C/C++ bundle, or installed the C/C++ plugin yourself.  If you are unsure whether your NetBeans IDE supports C/C++ development, follow the instruction on <a href="https://netbeans.org/community/releases/70/cpp-setup-instructions.html">this NetBeans community page</a> .</p>
	<ol>
		<li><p>Create a new project with the &#8220;C/C++ Project from Binary File&#8221; template.</p></li>
		<li><p>Set the &#8220;Binary File&#8221; to <code>/home/amos/miniat/out/exe/miniat_console</code> and the &#8220;Sources Root&#8221; to <code>/home/amos/miniat</code> before clicking &#8220;Next&#8221;.</p></li>
		<li><p>Choose a project name and location of your preference (we&#8217;ll just take the defaults) and &#8220;Finish&#8221;.</p></li>
	</ol>
	<p>Now that the project exists, we just need to adjust a few of the default settings from the &#8220;Project Properties&#8221; window to get NetBeans to work with our SCons build system and run/debug the <strong>MiniAT</strong> Console.</p>
	<ol>
		<li><p>Under the &#8220;Build -> Make&#8221; category, set the cuild command to <code>scons --debugging</code>, and the clean command to <code>scons -c</code>.</p></li>
		<li><p>From the &#8220;Run&#8221; category, set the run command to <code>"${OUTPUT_PATH}" ~/miniat/out/bin/simple_term.bin</code>, add the variable <code>LD_LIBRARY_PATH</code> to the environment with the value <code>/home/amos/miniat/out/lib</code>, and set the console type to <code>External Terminal</code>.</p></li>
		<li><p>Also, from the &#8220;Run&#8221;, you might choose to enable the &#8220;Build First&#8221; feature to ensure you are always testing the most recent changes to your sources.</p></li>
	</ol>
	<p>To test your setup, switch to the editor, add a breakpoint somewhere in <code>system/console/console.c</code>, clean and build, and click the debug button in the toolbar (or press Ctrl+F5).</p>
	<h3>Debugging with Eclipse</h3>
	<p>These insructions were written for Eclipse version 3.8.1, so the precise details necessary for this to work for you may vary slightly from the text below.  It is further assumed you have already installed C/C++ project suport through the CDT either because you initially downloaded the Eclipse with C/C++ bundle, or installed the CDT plugin yourself.</p>
	<ol>
		<li>Create a new empty C project with the project name <code>miniat</code>, but do not use the default location.  Instead, use <code>/home/amos/miniat</code>.  Additionally, if you have the option, switch the toolchain to &#8220;Linux GCC&#8221; before clicking &#8220;Finish&#8221;.</li>
	</ol>
	<p>Now that the project exists, we just need to adjust a few of the default &#8220;Project Settings&#8221; to get Eclipse to work with our SCons build system and run/debug the <strong>MiniAT</strong> Console.</p>
	<ol>
		<li><p>From the &#8220;C/C++ Build&#8221; screen, change the configuration to &#8220;[ All configurations ]&#8221;.</p></li>
		<li><p>In the &#8220;Builder Setting tab from that same screen, deselect &#8220;Use default build command&#8221;, set the build command to <code>scons</code>, disable automatic Makefile generation, and remove the <code>/Debug</code> from the end of the build directory.</p></li>
		<li><p>from the &#8220;Behavior&#8221; tab on that screen, set the incremental build command option to <code>--debugging</code> and the &#8220;clean&#8221; option to <code>-c</code>.</p></li>
		<li><p>Using the &#8220;Run/Debug Settings&#8221; screen, add a new C/C++ application launch configuration.  Set the C/C++ application to <code>/home/amos/miniat/out/exe/miniat_console</code> with the argument <code>/home/amos/miniat/out/bin/simple_term.bin</code>, and add the environment variable
			<code>LD_LIBRARY_PATH</code> with the value <code>/home/amos/miniat/out/lib</code>.</p></li>
	</ol>
	<p>To test your setup, switch to the editor, add a breakpoint somewhere in <code>system/console/console.c</code>, clean and build, and click the debug button in the toolbar.</p>
</div>

  