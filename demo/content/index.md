title: MiniAT -=- Home Page
	   
<div class="container">
      <div class="page-header">
        <div id="miniat-home-header"><img src="/images/logo_web.png" class="img-responsive" />        <h1>
          The Virtual Embedded Architecture Project        </h1>
        </div> 
      </div>
</div>  
<div class="container">
	<div class="row"> 
		<div class="col-md-6 col-xs-12">
			<a href="/download">
			<img src="/images/icon-clr-arrow-down.png" class="pull-left img-responsive" 
				onmouseover="this.src='/images/icon-clr-shadow-arrow-down.png'" 
				onmouseout="this.src='/images/icon-clr-arrow-down.png'" /></a>
			<p>Download the <strong>MiniAT</strong>, an open-source embedded microcontroller packaged as an easy to use C library, designed for and by computer science students.</p>
		</div>
	<div class="col-md-6 col-xs-12">
		<a href="/documentation">
		<img src="/images/icon-clr-docs.png" class="pull-left img-responsive" 
			onmouseover="this.src='/images/icon-clr-shadow-docs.png'" 
			onmouseout="this.src='/images/icon-clr-docs.png'" /></a>
		<p>Read the documentation and learn how to prototype and simulate interactive systems of your own design.</p>
	</div>
	<div class="col-md-6 col-xs-12">
		<a href="/getting-started">
		<img src="/images/icon-clr-bulb.png" class="pull-left img-responsive" 
			onmouseover="this.src='/images/icon-clr-shadow-bulb.png'" 
			onmouseout="this.src=/images/icon-clr-bulb.png'" /></a>
		<p>Get started with the <strong>MiniAT</strong> using these easy to follow tutorials.  They'll give you plenty of ideas for your own projects.</p></div>
	<div class="col-md-6 col-xs-12">
		<a href="/develop">
		<img src="/images/icon-clr-gear.png" class="pull-left img-responsive" 
			onmouseover="this.src='/images/icon-clr-shadow-gear.png'" 
			onmouseout="this.src='/images/icon-clr-gear.png'" /></a>
		<p>The <strong> MiniAT</strong> simulator and its development toolchain are all open source.  Whenever you're ready, we can't wait for you to contribute your own talent to make the <strong> MiniAT</strong> even better.</p></div>
	</div>
</div>
<div class="panel panel-info">
  <div class="panel-body">
<p style="text-align:justify;">The <strong>MiniAT</strong> is an open-source embedded microcontroller packaged as an easy to use C library, designed for and with computer science students.  Using any C-friendly language and the <strong>MiniAT</strong> development toolchain, users prototype and simulate interactive systems of their own design.  The <strong>MiniAT</strong> interface is extensible and flexible, so you can even use "off-the-shelf" peripherals or just enjoy the existing systems provided by the project and its community.</p>
  </div>
</div>
<p style="margin: 50px 15%; text-align: center;">
The <strong>MiniAT</strong> project is primarily administered by students, faculty, and alumni of the <a href="http://www.sunyit.edu">State University of New York Polytechnic Institute (formerly SUNYIT)</a>.
</p>
   
