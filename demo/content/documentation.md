title: MiniAT | Documentation    

<div class="container">
    <div class="page-header">
		<h1> Documentation </h1>         
    </div>
	<div id="sitemap_nav">
		<p><strong><a href="/documentation" >Documentation</a></strong></p>
	</div>
	<h2>Where&#8217;s the documentation???</h2>
	<p>How can an architecture simulator get by without any comprehensive documentation?
	It was easy&#8230; why, it was almost as if we did nothing at all.
	&#8230;OK, so there is some documentation, but it&#8217;s in the source distribution,
	and we fell into the trap so many engineers fall into, we had design/coding/playing tunnel
	vision for a long time. 
	<span class="glyphicon glyphicon-thumbs-down"></span></p>
	<p>We <strong><em>really</em></strong> need to get this part of the project together, and we know it.</p>
</div>
