title: MiniAT | Download    
	   
<div class="container">
    <div class="page-header">
                <h1>       Download        </h1>
    </div>
	<div id="sitemap_nav">
		<p><strong><a href="/download" >Download</a></strong></p>
	</div>
	<p>At the moment, there are no binary packages of the <strong>MiniAT</strong> ready to go, but this is a priority on our TODO list.  In the meantime, you can download the MiniAT in an easy to build source bundle.</p>
	<p>Included in the source download is:</p>
	<ul>
		<li>the core <strong>MiniAT</strong> microcontroller simulator library</li>
		<li>an example system, <em>miniat_console</em>, with terminal and keyboard</li>
		<li>the <em>mash</em> assembler for the <strong>MiniAT</strong></li>
		<li>an example assembly program, <em>simple_term.asm</em>, for the <em>miniat_console</em> system that echoes back anything you&#8217;ve typed whenever you press enter</li>
		<li>some documentation (mostly bits and bobs used by the development team)</li>
		<li>and example peripheral implementations to demonstrate how one can develop reusable peripherals with the <strong>MiniAT</strong></li>
	</ul>
	<h2>Source ZIP</h2>
	<p>Unless you plan on contributing back to the <strong>MiniAT</strong> community or prefer to use a revision control tool, you can simply download the most recent state of the sources from Bitbucket using the link below.</p>
	<p><a class="btn btn-primary btn-lg" role="button" href="http://bitbucket.org/miniat/0x1-miniat/get/master.zip"><span class="glyphicon glyphicon-cloud-download"></span> Download the source ZIP</a></p>
	<h2>Clone the Source with Git</h2>
	<p>If you already use <a href="http://git-scm.com">Git</a> or are comfortable with revision control systems of another persuasion, you can get the most recent source and entire project history from Bitbucket, our source&#8217;s host.</p>
	<p><a class="btn btn-primary btn-lg" role="button" href="http://bitbucket.org/miniat/0x1-miniat">
		<span class="glyphicon glyphicon-user" style="font-size:50%; letter-spacing:-5px;"></span>
		<span class="glyphicon glyphicon-user" style="font-size:75%; letter-spacing:-5px;"></span>
		<span class="glyphicon glyphicon-user" style="font-size:100%; letter-spacing:-5px;"></span>
		<span class="glyphicon glyphicon-user" style="font-size:75%; letter-spacing:-5px;"></span>
		<span class="glyphicon glyphicon-user" style="font-size:50%;"></span>
		Clone the source from Bitbucket</a>
	</p>
	<p>&#8230;or, for the impatient&#8230; <code>git clone https://bitbucket.org/miniat/0x1-miniat.git miniat</code></p>
</div>

    