﻿
import os
import datetime
import find_root
import shutil 
import errno


def run(name):
    name=os.getcwd()+os.sep+name

    if not os.path.exists(name):
        dir=find_root.site_directory()
        if dir == None:
            make_dir(name)          
        else: print(" Your are locating inside of an existing project. ", dir,"\n Please change your path and try again  ")
    else: print("Name already exist")

def copy_demo(src, dest):

    if not os.path.isdir(dest):
        os.makedirs(dest)
    try:
        shutil.copytree(src, dest)
    except OSError as e:

        # If the error was caused because the source wasn't a directory
        if e.errno == errno.ENOTDIR:
            shutil.copy(src, dest)
        else:
            print('Directory not copied. Error: %s' % e)

def make_dir(name):
    os. makedirs(name)   

    #.ssg file

    file = open(name+'/.ssg', 'a+')
    file.write("Time Created:"+  str(datetime.datetime.now())+"\n")   
    file.write("Ignore Extensions: [.md, .git, .ssg]\n")
    file.write("Ignore Files:  []\n")
    file.write("Default Header:  [themes\\Header.html]\n")
    file.write("Deafault Footer:  [themes\\Footer.html]\n")
    file.write("NavBar:  [themes\\NavBar.html]\n")
    file.write("Default Theme: default \n")
    file.write("Error Page: 404.html \n")
    file.close()
    
    #dir_from=find_root.project_directory(os.path.abspath(__file__))
    #for root, subFolder, files in os.walk(dir_from+os.sep+"demo"):
   
    for root, subFolder, files in os.walk(os.getcwd()+os.sep+"demo"):
             
        for item in files:
          
            tocopy=os.path.dirname(str(os.path.join(str(root),item)))  
            tocopy=tocopy.split("demo")[1]
            copy_demo(str(os.path.join(str(root),item)), name+os.sep+tocopy)
  


   